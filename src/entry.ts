import program from 'commander';
import path from 'path';
import fs from 'fs';
import {Worker} from 'worker_threads';
import scrape, * as scrapers from './scrape';
import {promiseStatus} from 'promise-status-async';

export interface Target {
	url: string;
	depth: number;
}

const queue: Target[] = [];

export interface Options {
	first: string; // The first page to scrape.
	depth: number; // How many pages deep can we go? If 0, no limit.
	modules: string; // Unused
	maxSize: number; // Maximum size to pull down. If 0, no limit.
	outFile: string; // Where to output the scraped data
	outDir: string; // Where to output what we find
}

let options: Options;

if (module.parent) {
	throw new Error('library mode is unimplemented');
} else { // Running as CLI
	program
		.requiredOption('-f, --first <url>', 'The first URL to target.')
		.option('-d, --depth <number>', 'How deep the rabbit hole goes', '2')
		.option('-s, --maxSize <size>', 'Maximum single file size we\'ll download in kilobytes.', '3000')
		.option('-o, --outFile <path>', 'The database file to use', './result.json')
		.option('-O, --outDir <path>', 'The database file to use', './results');
	program.parse(process.argv);
	options = {
		first: program.first,
		depth: program.depth,
		modules: program.modules,
		maxSize: program.maxSize,
		outFile: program.outFile,
		outDir: program.outDir
	};
}

export interface Result {
	url: string;
	references: string[];
	type: string;
	size: number;
}

const results: Result[] = [];

(async () => {
	console.log('Setting up...');
	await scrapers.init(options);
	console.log('Scraping...');
	queue.push({url: options.first, depth: 0});

	const awaited: Array<Promise<void>> = [];
	let index = 0;
	let done = false;

	while (true) {
		done = true;
		let running = 0;
		for (const i of awaited) {
			const status = await promiseStatus(i);
			if (status === 'pending') {
				done = false;
				running++;
			}
		}

		if (running >= require('os').cpus().length / 2) {
			await new Promise(resolve => setTimeout(resolve, 100));
		} else if (index >= queue.length) {
			if (done) {
				break;
			} else {
				await new Promise(resolve => setTimeout(resolve, 1000));
			}
		} else {
			const depth = queue[index].depth;
			running++;
			if (depth < options.depth) {
				let known = false;
				const ourUrl = new URL(queue[index].url);
				for (const i of results) {
					const theirUrl = new URL(i.url);
					known = known ? known : ourUrl.toString() === theirUrl.toString();
				}

				if (!known) {
					awaited.push(scrape(queue[index].url, options, {
						target: (url: string, depth: number) => {
							queue.push({url, depth});
						},
						result: (result: Result) => {
							results.push(result);
						},
						done: () => {} // Unused
					}, queue[index].depth));
				}
			}

			index++;
		}
	}

	console.log(`Writing results to ${options.outFile}`);

	let data;
	try {
		data = JSON.parse(fs.readFileSync(options.outFile).toLocaleString());
	} catch {
		data = {};
	}

	for (const result of results) {
		data[result.url] = Object.assign(data[result.url] || {}, result);
	}

	fs.writeFileSync(options.outFile, JSON.stringify(data));

	console.log('Done!');
	process.exit(0);
})();

