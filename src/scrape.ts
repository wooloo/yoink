import {Result, Options} from './entry';
import path from 'path';
import * as scrapers from './modules/shim';

export interface Scraper {
	understands: (url: string, options: Options) => Promise<boolean>;
	priority: number;
	scrape: (url: string, options: Options) => Promise<Result>;
	init: (options) => Promise<void>;
}

export async function init(options: Options) {
	for (const i in scrapers) {
		await scrapers[i].init();
	}
}

export interface ScrapeCallbacks {
	target: (url: string, depth: number) => void;
	result: (result: Result) => void;
	done: () => void;
}

export default async function scrape(url: string, options: Options, callbacks: ScrapeCallbacks, depth: number) {
	try {
		console.log(`Scraping ${url}, ${depth} layer${depth === 1 ? '' : 's'} deep...`);
		let scraper: Scraper;
		for (const i in scrapers) {
			const considered: Scraper = scrapers[i];
			const understood = await considered.understands(url, options);
			const better = !scraper || considered.priority > scraper.priority;
			if (understood && better) {
				scraper = considered;
			}
		}

		if (scraper) {
			const result = await scraper.scrape(url, options);
			for (const i of result.references) {
				callbacks.target(i, depth + 1);
			}

			callbacks.result(result);
		}
	} finally {
		callbacks.done();
	}
}

