import {Result, Options} from '../entry';

import {Scraper, init} from '../scrape';

import fetch from 'node-fetch';

import Http from 'http';
import puppeteer, {Browser} from 'puppeteer';
import path from 'path';
import {URL} from 'url';
import mkdirp from 'mkdirp';

const UNDERSTOOD_TYPES: Set<string> = new Set([
	'text/html',
	'text/plain'
]);

let browser: Browser;

const html: Scraper = {
	priority: 0,

	async init() {
		browser = await puppeteer.launch();
	},

	async understands(url: string, options: Options): Promise<boolean> {
		try {
			const http = url.startsWith('http');
			let small = false;
			let parsable = false;

			if (http) {
				const Url = new URL(url);

				// Const head = await fetch(url, {
				// 	method: 'HEAD'
				// });
				// small = Number.parseInt(head.headers.get('Content-Length'), 10) / 1000 < options.maxSize;
				// parsable = UNDERSTOOD_TYPES.has(head.headers.get('Content-Type'));
				const response: any = await new Promise((resolve, reject) => {
					const request = Http.request({
						method: 'HEAD',
						host: Url.hostname,
						path: Url.pathname
					}, resolve);
					request.on('error', reject);
					request.end();
				});
				small = (Number.parseInt(response.headers['content-length'], 10) || 0) / 1000 < options.maxSize;
				UNDERSTOOD_TYPES.forEach(value => {
					parsable = parsable ? parsable : response.headers['content-type'].startsWith(value);
				});
			}

			return http && small && parsable;
		} catch (error) {
			console.error(error);
			return false;
		}
	},
	async scrape(url: string, options: Options): Promise<Result> {
		let refs: string[] = [];
		let size;
		let type;
		try {
			const page = await browser.newPage();
			size = await getSize(url);
			type = await getType(url);
			page.on('request', request => {
				refs.push(request.url());
			});
			await page.goto(url, {});
			const href: string[] = await page.$$eval('a', as => as.map(a => a.getAttribute('href')));
			refs = refs.concat(href);

			const ourUrl = new URL(url);

			refs = refs.filter(value => {
				try {
					const theirUrl = new URL(value);
					return ourUrl.toString() !== theirUrl.toString();
				} catch {
					return false;
				}
			});
			await mkdirp(path.join(options.outDir,
				ourUrl.hostname,
				ourUrl.pathname
					.split('/')
					.slice(0, -1)
					.join('/')));
			const shot = path.join(options.outDir,
				ourUrl.hostname,
				ourUrl.pathname,
				ourUrl.pathname.endsWith('/') ? 'index' : ''
			) + '.png';
			await page.screenshot({
				path: shot
			});
			return {
				url,
				references: refs,
				type,
				size
			};
		} catch {
			return {
				url,
				references: [],
				type: type || 'application/octet-stream',
				size: size || 0
			};
		}
	}
};

async function getSize(url: string) {
	const head = await fetch(url, {
		method: 'HEAD'
	});
	return Number.parseInt(head.headers.get('Content-Length'), 10);
}

async function getType(url: string) {
	const head = await fetch(url, {
		method: 'HEAD'
	});
	return head.headers.get('Content-Length');
}

export default html;
